Environment
===========
* Language: PHP
* Framework: Yii2
* Database: MySQL
* Domain: postshelter.com
* Hosting: Heroku (temporary)

Composer installation (global)
---------------------
<pre>
$ curl -sS https://getcomposer.org/installer | php
$ mv composer.phar /usr/local/bin/composer
</pre>

**Notice**

<pre>
Could not fetch https://api.github.com/repos/jquery/sizzle, please create a GitHub OAuth token to go over the API rate limit
Head to https://github.com/settings/tokens/new?scopes=repo&description=Composer+on+Trungs-MacBook-Pro.local+2015-06-20+1348
to retrieve a token. It will be stored in "/Users/TrungVT/.composer/auth.json" for future use by Composer.
</pre>
