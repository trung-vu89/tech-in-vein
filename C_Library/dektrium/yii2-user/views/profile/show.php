<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = isset($profile) ? (empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name)) : "Unknown";
?>
<?php if(isset($profile)): ?>
<div class="row list-group">
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <img src="http://gravatar.com/avatar/<?= $profile->gravatar_id ?>?s=230" alt="" class="img-rounded img-responsive" />
            </div>
            <div class="col-sm-6 col-md-8">
                <div class="list-group"><h4><?= $this->title ?></h4></div>
                <ul style="padding: 0; list-style: none outside none;">
                    <li><i class="glyphicon glyphicon-map-marker text-muted"></i> <?= empty($profile->location) ? "Unknown" : Html::encode($profile->location) ?></li>
                    <li><i class="glyphicon glyphicon-globe text-muted"></i> <?= empty($profile->website) ? "Unknown" : Html::a(Html::encode($profile->website), Html::encode($profile->website)) ?></li>
                    <li><i class="glyphicon glyphicon-envelope text-muted"></i> <?= empty($profile->public_email) ? "Unknown" : Html::a(Html::encode($profile->public_email), 'mailto:' . Html::encode($profile->public_email)) ?></li>
                    <li><i class="glyphicon glyphicon-time text-muted"></i> <?= Html::encode(date('Y-m-d H:i', $profile->user->created_at)); ?></li>
                </ul>
                <?php if (!empty($profile->bio)): ?>
                    <p><?= Html::encode($profile->bio) ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php else: ?>
<div class="jumbotron">User information is not available.</div>
<?php endif; ?>
