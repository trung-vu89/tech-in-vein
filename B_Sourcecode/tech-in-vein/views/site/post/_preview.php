<?php

use yii\helpers\Html;

?>
<div class='panel panel-default'>
    <div class='panel-body'>
		<h2><label id="preview-title-label" style="font-weight:bold; padding-left: 10px; padding-right: 10px; word-wrap: break-word;"></label></h2>
        <hr class="style-two">
		<br/>
		<label id="preview-content-label" style="font-size:14pt; color:black; padding-left: 10px; padding-right: 10px; word-wrap: break-word;"></label>
	</div>
</div>
