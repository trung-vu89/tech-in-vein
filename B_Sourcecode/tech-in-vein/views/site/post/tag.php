<?php

$this->title = "Posts with " . Yii::$app->request->getQueryParam('name') . " tag";

?>

<div class="row">
    <div class="col-md-7 col-md-offset-2">
        <div id="main-post-list">
            <ul class="list-unstyled">
                <?php if (isset($postsByTag) && count($postsByTag) > 0): ?>
                    <?php foreach ($postsByTag as $post): ?>
                        <li>
                            <?= $this->render('/site/post/_item', [
                                'post' => $post
                            ]) ?>
                        </li>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h4 class="well" style="width: 100%; margin: 0 auto; text-align: center;">
                        No data
                    </h4>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
