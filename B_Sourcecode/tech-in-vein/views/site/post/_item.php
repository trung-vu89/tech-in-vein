<?php

use app\models\Post;
use yii\helpers\Html;

$postDetailUrl = '/post/view?id=' . $post->id;

?>

<div class='panel panel-default'>
	<div class='panel-body' style="padding-left: 3%; padding-right: 3%;">
		<?php if (!empty($post) && $post->type == Post::TYPE_QUESTION): ?>
			<div>
				<p class="label label-warning">Need help</p>
			</div>
		<?php endif; ?>
		<a href="<?= $postDetailUrl ?>" style="font-weight: bold; font-size: 24pt; color: #444444; text-decoration: none; word-wrap: break-word; width: auto;">
			<?php if (!empty($post)) echo $post->title; ?>
		</a>
		<br/>
		<a href="/user/profile?id=<?= $post->user->id ?>"><?= $post->user->username ?></a> ・
		<?= Html::encode(date('F jS, Y', $post->updated_at)); ?> ・
		<a href="<?= $postDetailUrl ?>"><?= count($post->comments) . " comments" ?></a>
		<?php if (!empty($post->getTags()) && count($post->getTags()) > 0): ?>
			<div>
	            <?php foreach ($post->getTags() as $tag): ?>
	                <a href="/post/tag?name=<?= $tag; ?>" class="label label-info"><?php echo $tag; ?></a>
	            <?php endforeach; ?>
			</div>
			<br/>
		<?php else: ?>
			<br/>
			<br/>
		<?php endif; ?>
		<a href="<?= $postDetailUrl ?>" style="line-height: 30px; color: #4A4A4A; text-decoration: none; font-size: 12pt; word-wrap: break-word;">
			<?php
				if (isset($post))
				{
					if (strlen($post->content) <= 256)
					{
						echo $post->content;
					}
					else
					{
						$originContentString = strip_tags($post->content, '<pre>');
						$originContentString = substr($originContentString, 0, 255);
						while (substr_count($originContentString, '<p>') > 5)
						{
							$originContentString = substr($originContentString, 0, strlen($originContentString) - 2);
						}
						echo $originContentString . "...";
					}
				}
			?>
		</a>
		<br/>
		<a href="<?= $postDetailUrl ?>" class="text-primary" style="text-decoration: none;">Read more</a>
	</div>
</div>
