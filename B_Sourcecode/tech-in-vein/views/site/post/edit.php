<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

$this->title = (isset($mode) && $mode == 'create') ? "Create new post" : "Edit post";

$onEditJs = <<< 'SCRIPT'

$(function () {
    // Post content edit tips
    $("[data-toggle='tooltip']").tooltip();

    // Init content ckeditor & update text into Content preview
    $("#edit-content-textbox").redactor({
        changeCallback: function()
        {
            // code here
        }
    });

    // Form Reset button click event
    $("#post-reset-button").click(function() {
        $("#modal-title-label").text('Reset form');
        $("#modal-body-label").text('Are you sure want to clear all information in this form?');
        $("#modal-confirm-button").click(function() {
            $("#edit-title-textbox").val("");
            $("#edit-content-textbox").redactor('code.set', '');
            $("#edit-tag-textbox").val("");
            $("#edit-scope-dropdownlist").val("0");
            $('input[id="edit-type-selection"]').val([0]);
            $("#edit-comment-dropdownlist").val("0");
            $("#modal-confirm-dialog").modal('hide');
        });
        $("#modal-confirm-dialog").modal('toggle');
    });

    // Preview tab selected event
    $('a[href="#w0-tab1"]').on('shown.bs.tab', function (e) {
        var previewTitleLabel = document.getElementById("preview-title-label");
        var editTitleLabel = document.getElementById("edit-title-textbox");
        previewTitleLabel.innerHTML = editTitleLabel.value;

        var previewContentLabel = document.getElementById("preview-content-label");
        var editContentLabel = document.getElementById("edit-content-textbox");
        previewContentLabel.innerHTML = editContentLabel.value;
    });

    // Publish button click event
    $("#post-submit-button").click(function() {
        $("#modal-title-label").text('Publish this article');
        $("#modal-body-label").text('Are you sure want to publish this article right now?');
        $("#modal-confirm-button").click(function() {
            var statusHiddenValue = document.getElementById("edit-status-value");
            statusHiddenValue.value = "0";
            document.getElementById("post-create-form").submit();
        });
        $("#modal-confirm-dialog").modal('toggle');
    });

    // Save Draft button click event
    $("#post-draft-button").click(function() {
        $("#modal-title-label").text('Save article as a draft');
        $("#modal-body-label").text('Are you sure want to save this article as a draft?');
        $("#modal-confirm-button").click(function() {
            var statusHiddenValue = document.getElementById("edit-status-value");
            statusHiddenValue.value = "1";
            document.getElementById("post-create-form").submit();
        });
        $("#modal-confirm-dialog").modal('toggle');
    });


    $("#post-image-input-file").change(function() {
        var input = $(this), numFiles = input.get(0).files ? input.get(0).files.length : 1, label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

        var postImageUploadProgressBar = document.getElementById("post-image-upload-progress-bar");
        if (input.get(0).files.length > 0)
        {
            postImageUploadProgressBar.setAttribute("style", "");
            var imageData = new FormData();
            $.each(input.get(0).files, function(key, value) {
                imageData.append(key, value);
            });

            $.ajax({
                url: "/image/upload",
                type: "POST",
                data:  imageData,
                dataType: 'json',
                contentType: false,
                processData:false,
                cache: false,
                success: function(data) {
                    // success
                    postImageUploadProgressBar.setAttribute("style", "display: none;");
                    $("#edit-content-textbox").redactor('insert.html', '<br><p class="text-center"><img src="' + data.imageUrl + '" style="max-width:100%; max-height:100%;"></p><br>');
                },
                error: function(xhr, errorStatus, errorThrown){
                    // error
                    postImageUploadProgressBar.setAttribute("style", "display: none;");
                    alert("Something wrong! The image has not been uploaded yet! Please try again.");
                }
            });
        }
    });

    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : '  ' + label;
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
    });
});

SCRIPT;
$this->registerJs($onEditJs);

?>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?php

            echo Tabs::widget([
                'items' => [
                    [
                        'label' => 'Edit',
                        'content'=> $this->render('_edit', ['model' => $model]),
                        'active' => true,
                        'data-toggle' => 'edit',
                    ],
                    [
                        'label' => 'Preview',
                        'content' => $this->render('_preview', ['model' => $model]),
                        'data-toggle' => 'preview',
                    ],
                ],
                'options' => ['class' => 'nav nav-tabs nav-justified']
            ]);
        ?>

    </div>
</div>
