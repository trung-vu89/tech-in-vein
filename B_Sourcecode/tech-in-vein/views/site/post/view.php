<?php

use yii\helpers\Html;
use app\models\Post;

if (!empty($model)) $this->title = $model->title;

$onPostDetailJS = <<< 'SCRIPT'

$(function() {
    $("#post-detail-publish-button").click(function() {
        $("#modal-title-label").text('Publish this article');
        $("#modal-body-label").text('Are you sure want to publish this article right now?');
        $("#modal-confirm-button").click(function() {
            $.ajax({
                type: "PUT",
                url: '/post/publish',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({post_id: getParameterByName("id")}),
                success: function(data) {
                    window.location.replace("/post/manage");
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Something wrong! Cannot publish this article. Please try again.");
                }
            });
            $("#modal-confirm-dialog").modal('hide');
        });
        $("#modal-confirm-dialog").modal('toggle');
    });

    $("#post-detail-edit-button").click(function() {
        var postId = document.getElementById("post-id").value;
        window.location.replace("/post/edit?id=" + postId);
    });

    $("#post-detail-delete-button").click(function() {
        $("#modal-title-label").text('Delete this article');
        $("#modal-body-label").text('Are you sure want to delete this article?');
        $("#modal-confirm-button").click(function() {
            $.ajax({
                type: "DELETE",
                url: '/post/delete',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({post_id: getParameterByName("id")}),
                success: function(data) {
                    window.location.replace('/post/manage');
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Something wrong! Cannot delete this article. Please try again.");
                }
            });
            $("#modal-confirm-dialog").modal('hide');
        });
        $("#modal-confirm-dialog").modal('toggle');
    });
});

SCRIPT;
$this->registerJs($onPostDetailJS);

?>

<input type="hidden" id="post-id" value="<?= $model->id; ?>">

<div class="row">
    <div class="col-md-2">
        <div class='panel panel-default'>
            <div class="panel-heading">
                <h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
                    <i class="glyphicon glyphicon-user"></i> &nbsp;Author
                </h3>
            </div>
            <div class='panel-body'>
                <a href="/user/profile?id=<?= $model->user->id ?>"><img src="http://gravatar.com/avatar/<?= $profile->gravatar_id ?>?s=230" class="img-circle" /></a>
                <div style="text-align: center; font-size: 13pt; padding-top: 5%;"><a href="/user/profile?id=<?= $model->user->id ?>"><?= $model->user->username ?></a></div>
                <div style="text-align: center; padding-top: 5%;">
                    at <?= Html::encode(date('F jS, Y', $model->updated_at)); ?>
                </div>
            </div>
        </div>
        <div class='panel panel-default'>
            <div class="panel-heading">
                <h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
                    <i class="glyphicon glyphicon-tags"></i> &nbsp;Tags
                </h3>
            </div>
            <div class='panel-body' style="text-align: center;">
                <?php if (!empty($model->getTags()) && count($model->getTags()) > 0): ?>
                    <?php foreach ($model->getTags() as $tag): ?>
                        <a href="/post/tag?name=<?= $tag; ?>" class="label label-info"><?php echo $tag; ?></a>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div style="text-align: center;">No tags</div>
                <?php endif; ?>
            </div>
        </div>
        <?php if (!Yii::$app->user->isGuest && $model->user_id == Yii::$app->user->id): ?>
            <div class='panel panel-default'>
                <div class="panel-heading">
                    <h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
                        <i class="glyphicon glyphicon-edit"></i> &nbsp;Customize
                    </h3>
                </div>
                <div class='panel-body' style="width: 80%; margin: 0 auto; text-align: center;">
                    <?php if ($model->status == Post::STATUS_DRAFT || $model->status == Post::STATUS_DELETE): ?>
                        <?= Html::button('Publish',['id' => 'post-detail-publish-button', 'class' => 'btn btn-primary', 'style' => 'width: 100%;']) ?>
                        <br/>
                        <br/>
                    <?php endif; ?>
                    <?php if ($model->status == Post::STATUS_DRAFT || $model->status == Post::STATUS_PUBLISH): ?>
                        <?= Html::button('Edit',['id' => 'post-detail-edit-button', 'class' => 'btn btn-warning', 'style' => 'width: 100%;']) ?>
                        <br/>
                        <br/>
                        <?= Html::button('Delete',['id' => 'post-detail-delete-button', 'class' => 'btn btn-danger', 'style' => 'width: 100%;']) ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-md-7">
    	<div class='panel panel-default'>
    		<div class='panel-body' style="padding-left: 5%; padding-right: 5%;">
                <?php if ($model->status == Post::STATUS_DRAFT): ?>
                    <div class="label label-warning">Draft</div>
                <?php elseif ($model->status == Post::STATUS_DELETE): ?>
                    <div class="label label-danger">In Trash</div>
                <?php endif; ?>
                <?php if ($model->type == Post::TYPE_QUESTION): ?>
                    <div class="label label-warning">Need help</div>
                <?php endif; ?>
    			<h2><label style="font-weight:bold;">
    				<?php if (!empty($model)) echo $model->title; ?>
    			</label></h2>
    			<hr class="style-two">
                <br/>
				<label class="post-content-text" style="word-wrap: break-word; line-height: 30px;">
					<?php if (!empty($model)) echo $model->content; ?>
				</label>
    		</div>
    	</div>
        <?php if ($model->status == Post::STATUS_PUBLISH): ?>
            <div class='panel panel-default'>
        		<div class='panel-body' style="padding-left: 5%; padding-right: 5%;">
                    <?= $this->render('/site/comment/_edit', [
                        'comments' => $model->comments
                    ]); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-md-3">
        <div class='panel panel-default'>
            <div class="panel-heading">
                <h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
                    <i class="glyphicon glyphicon-folder-open"></i> &nbsp;Related <?= $model->type == Post::TYPE_ARTICLE ? 'Articles' : 'Questions'; ?>
                </h3>
            </div>
            <div class='panel-body'>
                <ul>
                    <?php foreach ($relatedPosts as $relatedPost): ?>
                         <li><a href="/post/view?id=<?= $relatedPost->id ?>"><span style="overflow: hidden; font-size: 11pt; word-wrap:break-word;"><?= $relatedPost->title ?></span></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
