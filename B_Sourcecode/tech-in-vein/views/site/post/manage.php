<?php

use \app\models\Post;

$this->title = "Post Management";

$publishList = [];
$draftList = [];
$deleteList = [];

if (isset($posts))
{
    foreach ($posts as $post) {
        if ($post->status == Post::STATUS_PUBLISH)
        {
            array_push($publishList, $post);
        }
        elseif ($post->status == Post::STATUS_DRAFT) {
            array_push($draftList, $post);
        }
        else {
            array_push($deleteList, $post);
        }
    }
}

$onPostManageJs = <<< 'SCRIPT'

$(function () {
    $("#post-manage-publish-link").click(function() {
        $("#post-manage-publish-container").attr('class', 'active');
        $("#post-manage-draft-container").attr('class', '');
        $("#post-manage-delete-container").attr('class', '');
        $("#post-manage-publish-list").attr('style', '');
        $("#post-manage-draft-list").attr('style', 'display: none;');
        $("#post-manage-delete-list").attr('style', 'display: none;');
    });

    $("#post-manage-draft-link").click(function() {
        $("#post-manage-publish-container").attr('class', '');
        $("#post-manage-draft-container").attr('class', 'active');
        $("#post-manage-delete-container").attr('class', '');
        $("#post-manage-publish-list").attr('style', 'display: none;');
        $("#post-manage-draft-list").attr('style', '');
        $("#post-manage-delete-list").attr('style', 'display: none;');
    });

    $("#post-manage-delete-link").click(function() {
        $("#post-manage-publish-container").attr('class', '');
        $("#post-manage-draft-container").attr('class', '');
        $("#post-manage-delete-container").attr('class', 'active');
        $("#post-manage-publish-list").attr('style', 'display: none;');
        $("#post-manage-draft-list").attr('style', 'display: none;');
        $("#post-manage-delete-list").attr('style', '');
    });
});;

SCRIPT;
$this->registerJs($onPostManageJs);

?>

<div class="row">
    <div class="col-md-2">
    	<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
                    <i class="glyphicon glyphicon-stats"></i> &nbsp;Statistics
                </h3>
			</div>
			<div class="panel-body">
				<ul class="nav nav-pills nav-stacked">
					<li id="post-manage-publish-container" class="active"><a id="post-manage-publish-link" href="#">
                        Publish
                        <span class="badge pull-right">
                            <?= isset($publishList) ? count($publishList) : 0;  ?>
                        </span>
                    </a></li>
					<li id="post-manage-draft-container"><a id="post-manage-draft-link" href="#">
                        Draft
                        <span class="badge pull-right">
                            <?= isset($draftList) ? count($draftList) : 0;  ?>
                        </span>
                    </a></li>
					<li id="post-manage-delete-container"><a id="post-manage-delete-link" href="#">
                        Delete
                        <span class="badge pull-right">
                            <?= isset($deleteList) ? count($deleteList) : 0;  ?>
                        </span>
                    </a></li>
				</ul>
			</div>
		</div>
    </div>
    <div class="col-md-7">
        <div id="post-manage-publish-list">
            <div id="main-post-list">
        		<ul class="list-unstyled">
                    <?php if (isset($publishList) && count($publishList) > 0): ?>
            			<?php foreach ($publishList as $publish): ?>
            				<li>
            					<?= $this->render('/site/post/_item', [
            						'post' => $publish
            					]) ?>
            				</li>
            			<?php endforeach; ?>
                    <?php else: ?>
                        <h4 class="well" style="width: 100%; margin: 0 auto; text-align: center;">
                            No data
                        </h4>
                    <?php endif; ?>
        		</ul>
        	</div>
        </div>
        <div id="post-manage-draft-list" style="display: none;">
            <div id="main-post-list">
        		<ul class="list-unstyled">
                    <?php if (isset($draftList) && count($draftList) > 0): ?>
            			<?php foreach ($draftList as $draft): ?>
            				<li>
            					<?= $this->render('/site/post/_item', [
            						'post' => $draft
            					]) ?>
            				</li>
            			<?php endforeach; ?>
                    <?php else: ?>
                        <h4 class="well" style="width: 100%; margin: 0 auto; text-align: center;">
                            No data
                        </h4>
                    <?php endif; ?>
        		</ul>
        	</div>
        </div>
        <div id="post-manage-delete-list" style="display: none;">
            <div id="main-post-list">
        		<ul class="list-unstyled">
                    <?php if (isset($deleteList) && count($deleteList) > 0): ?>
            			<?php foreach ($deleteList as $delete): ?>
            				<li>
            					<?= $this->render('/site/post/_item', [
            						'post' => $delete
            					]) ?>
            				</li>
            			<?php endforeach; ?>
                    <?php else: ?>
                        <h4 class="well" style="width: 100%; margin: 0 auto; text-align: center;">
                            No data
                        </h4>
                    <?php endif; ?>
        		</ul>
        	</div>
        </div>
    </div>
</div>
