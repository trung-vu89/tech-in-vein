<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use app\models\Post;

$scopeSelections = [
	Post::SCOPE_PUBLIC  => 'Everyone',
	Post::SCOPE_FRIEND  => 'Friends',
	Post::SCOPE_PRIVATE => 'Only me'
];

$typeSelections = [
	Post::TYPE_ARTICLE  => 'Article',
	Post::TYPE_QUESTION => 'Question'
];

$commentPermissionSelections = [
	Post::COMMENT_PUBLIC => 'Everyone',
	Post::COMMENT_FRIEND => 'Friends',
	Post::COMMENT_LOCK   => 'Lock'
];

$statusSelections = [
	Post::STATUS_PUBLISH => 'Publish',
	Post::STATUS_DRAFT	 => 'Save draft',
];

$method = (isset($model) && isset($model->id)) ? 'put' : 'post';
$action = (isset($model) && isset($model->id)) ? '/post/edit' : '/post/create';

?>

<div class='panel panel-default'>
    <div class='panel-body'>
	    <?php $form = ActiveForm::begin([
	        'id' => 'post-create-form',
	        'method' => $method,
	        'action' => [$action]
	    ]); ?>

		<?php if ((isset($model) && isset($model->id))): ?>
			<!-- Post Id -->
		    <?= $form->field($model, 'id')->hiddenInput(['id' => 'edit-id-value', 'value' => $model->id])->label(false); ?>
		<?php endif; ?>

	    <!-- Title -->
	    <?= $form->field($model, 'title')->textInput(['id' => 'edit-title-textbox','class' => 'form-control input-lg', 'placeholder' => 'Title', 'style' => 'font-weight: bold;'])->label(false); ?>

	    <!-- Content -->
	    <?= $form->field($model, 'content')->textArea(['id' => 'edit-content-textbox', 'placeholder' => 'Content', 'rows' => '5'])->label(false); ?>

		<div id="post-image-upload-progress-bar" style="display: none;" class="progress progress-striped active">
			<div class="progress-bar progress-bar-success" style="width: 100%"></div>
		</div>

		<div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-primary btn-file">
                    Add Images&hellip; <input id="post-image-input-file" name="post-image-input-file" type="file" accept="image/*">
                </span>
            </span>
            <input type="text" class="form-control" readonly>
        </div>
		<br/>
	    <a data-toggle="tooltip" data-placement="right" title="" data-original-title="Format the text in ¶ menu">Tips?</a>

	    <!-- Tags -->
	    <?= $form->field($model, 'tags')->textInput(['id' => 'edit-tag-textbox', 'class' => 'form-control input-sm', 'placeholder' => 'Tags (seperates with comma "," character)'])->label(false); ?>

	    <!-- Scope view -->
	    <?= $form->field($model, 'view_scope')->dropDownList($scopeSelections, ['id' => 'edit-scope-dropdownlist'])->label('Who can see this post?'); ?>

	    <!-- Type of post -->
	    <?= $form->field($model, 'type')->inline()->radioList($typeSelections, ['id' => 'edit-type-selection'])->label('Need help? Post a question to ask!') ?>

	    <!-- Comment permission -->
	    <?= $form->field($model, 'comment_permission')->dropDownList($commentPermissionSelections, ['id' => 'edit-comment-dropdownlist'])->label('Who can comment to this post?') ?>

	    <!-- Status of post -->
	    <?= $form->field($model, 'status')->hiddenInput(['id' => 'edit-status-value', 'value' => '0'])->label(false); ?>

	    <?php ActiveForm::end(); ?>

	    <ul class="pull-right list-inline">
	    	<?= Html::button('Publish', ['id' => 'post-submit-button', 'class' => 'btn btn-primary btn-lg btn-raised', 'style' => 'margin-right: 10px;']) ?>
	    	<?= Html::button('Save Draft', ['id' => 'post-draft-button', 'class' => 'btn btn-warning btn-lg btn-raised', 'style' => 'margin-right: 10px;']) ?>
	    	<?= Html::button('Reset', ['id' => 'post-reset-button', 'class' => 'btn btn-default btn-lg btn-raised']) ?>
		</ul>

    </div>
</div>
