<?php
use yii\widgets\menu;
?>

<div class="panel panel-default">
	<div class="panel-heading">
        <h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
            <i class="glyphicon glyphicon-globe"></i> &nbsp;Community
        </h3>
    </div>
	<div class="panel-body">
		<?php
			echo Menu::widget([
			    'items' => [
			        ['label' => 'All News', 'url' => ['site/index']],
			        ['label' => 'Articles', 'url' => ['#']],
			        ['label' => 'Questions', 'url' => ['#']],
			    ],
			    'options' => ['class' => 'nav nav-pills nav-stacked']
			]);
		?>
	</div>
</div>

<?php if(!Yii::$app->user->isGuest): ?>
<div class="panel panel-default">
	<div class="panel-heading">
        <h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
            <i class="glyphicon glyphicon-user"></i> &nbsp;Favourites
        </h3>
    </div>
	<div class="panel-body">
		<?php
			echo Menu::widget([
			    'items' => [
			        ['label' => 'My Owns', 'url' => ['site/index']],
			        ['label' => 'Bookmarks', 'url' => ['post/create']],
			    ],
			    'options' => ['class' => 'nav nav-pills nav-stacked']
			]);
		?>
	</div>
</div>
<?php endif; ?>

