<?php
use yii\widgets\menu;

$onEmailSubscribeJS = <<< 'SCRIPT'

$("#subscribe-submit-button").click(function() {
	var successText = document.getElementById("subscribe-success-text");
	var failedText = document.getElementById("subscribe-failed-text");
	var emailAddress = document.getElementById("subscribe-email-textbox").value;
	emailAddress = emailAddress.trim();
	if (emailAddress == "") {
		successText.setAttribute("style", "display: none;");
		failedText.setAttribute("style", "");
	} else {
		$.ajax({
			type: "POST",
			url: '/site/subscribe',
			data: JSON.stringify({email: emailAddress}),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(data) {
				failedText.setAttribute("style", "display: none;");
				successText.setAttribute("style", "");
				console.log("success");
			},
			error: function(xhr, errorStatus, thrown) {
				successText.setAttribute("style", "display: none;");
				failedText.setAttribute("style", "");
				console.log("failed");
			}
		});
	}
});

SCRIPT;

$this->registerJs($onEmailSubscribeJS);

?>

<div class="panel panel-default">
	<div class="panel-heading">
        <h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
            <i class="glyphicon glyphicon-envelope"></i> &nbsp;Email Subcription
        </h3>
    </div>
	<div class="panel-body">
		<div style="text-align: center;">
			<input id="subscribe-email-textbox" type="text" placeholder="Your email address..." style="width: 80%;">
			<br/>
			<p id="subscribe-failed-text" class="text-danger" style="display: none;">Email address subscription failed.</p>
			<p id="subscribe-success-text" class="text-success" style="display: none;">Email address subscription succeeded.</p>
			<br/>
			<input id="subscribe-submit-button" type="button" value="Subcribe" class="btn btn-danger" style="width: 50%;">
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
        <h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
            <i class="glyphicon glyphicon-time"></i> &nbsp;Update
        </h3>
    </div>
	<div class="panel-body">
		<ul>
			<?php foreach ($latestPosts as $latestPost): ?>
				 <li><a href="/post/view?id=<?= $latestPost->id ?>"><span style="overflow: hidden; font-size: 12pt; word-wrap:break-word;"><?= $latestPost->title ?></span></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
        <h3 class="panel-title" style="width: 100%; margin: 0 auto; text-align: center;">
            <i class="glyphicon glyphicon-list-alt"></i> &nbsp;Tags
        </h3>
    </div>
	<div class="panel-body">
		<ul class="nav nav-pills">
			<?php foreach ($topTags as $tag): ?>
				<li><a href="/post/tag?name=<?= $tag->name; ?>" style="font-size: 11pt;">
					<?= ucfirst($tag->name); ?>
					<span class="badge">
						<?= $tag->count;  ?>
					</span>
				</a></li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>
