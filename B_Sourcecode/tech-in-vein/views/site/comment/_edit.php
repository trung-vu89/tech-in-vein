<?php

use yii\helpers\Html;
use app\models\Comment;

$onCommentViewJs = <<< 'SCRIPT'

$(function () {
    $("#post-comment-edit-textarea").redactor({
        changeCallback: function() { },
    });

    $("#post-comment-reset-button").click(function() {
        $("#modal-title-label").text('Reset form');
        $("#modal-body-label").text('Are you sure want to clear all information in this form?');
        $("#modal-confirm-button").click(function() {
            $("#post-comment-edit-textarea").redactor('code.set', '');
            $("#modal-confirm-dialog").modal('hide');
        });
        $("#modal-confirm-dialog").modal('toggle');
    });

    $("#post-comment-reply-button").click(function() {
        $("#modal-title-label").text('Send Comment');
        $("#modal-body-label").text('Are you sure want to send this comment?');
        $("#modal-confirm-button").click(function() {
            $.ajax({
                type: "POST",
                url: '/post/comment',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({post_id: getParameterByName('id'), comment: document.getElementById("post-comment-edit-textarea").value}),
                success: function(data) {
                    location.reload();
                },
                error: function(xhr, textStatus, errorThrown) {
                    alert("Something wrong! You cannot post a comment. Please try again.");
                }
            });
            $("#modal-confirm-dialog").modal('hide');
        });
        $("#modal-confirm-dialog").modal('toggle');
    });
});

SCRIPT;
$this->registerJs($onCommentViewJs);

?>

<?php if (Yii::$app->user->isGuest): ?>
    <h4 class="well" style="width: 100%; margin: 0 auto; text-align: center;">
        Sign in as member to comment.
    </h4>
<?php else: ?>
    <form class="form-horizontal">
        <fieldset>
            <legend>Comment</legend>
            <div>
                <textarea id="post-comment-edit-textarea" placeholder="Comment..." name="content" rows="5" maxlength="50"></textarea>
            </div>
        </fieldset>
    </form>
    <ul class="pull-right list-inline">
        <button id="post-comment-reply-button" class="btn btn-primary" style="margin-right: 10px;">Reply</button>
        <button id="post-comment-reset-button" class="btn btn-default">Reset</button>
    </ul>
<?php endif; ?>
<br />
<br />
<hr>
<?php if (isset($comments) && count($comments)): ?>
    <table class="table table-striped table-hover "><tbody>
        <?php foreach ($comments as $comment): ?>
            <tr>
                <div class="row">
                    <div class="col-md-2" style="margin: 10px 0 10px 0;">
                        <a href="/user/profile?id=<?= $comment->user->id ?>" style="align-text: center;">
                            <img src="http://gravatar.com/avatar/<?= $comment->user->profile->gravatar_id ?>?s=230" class="img-circle pull-left" style="width: 60px; height: 60px;" />
                        </a>
                    </div>
                    <div class="col-md-10">
                        <div><a href="/user/profile?id=<?= $comment->user->id ?>" style="align-text: center;">
                            <h5 style="margin-left: -15px; padding-left: 0px; font-weight: bold;"><?php echo $comment->user->username; ?></h5>
                        </a></div>
                        <div><p style="margin-left: -15px; padding-left: 0px;"><?= Html::encode(date('F jS, Y | h:i:s A', $comment->created_at)); ?></p></div>
                    </div>
                </div>
                <div class="row">
                    <div class="well well-sm col-md-11" style="margin: 0 -10px 0 10px; word-wrap: break-word;">
                        <p class="post-content-text" style="margin-left: -20px; padding-left: 0px; font-size: 14pt;"><?= $comment->content; ?></p>
                    </div>
                </div>
                <hr>
            </tr>
        <?php endforeach; ?>
    </tbody></table>
<?php else: ?>
    <h4 class="well" style="width: 100%; margin: 0 auto; text-align: center;">
        No comments
    </h4>
<?php endif; ?>
