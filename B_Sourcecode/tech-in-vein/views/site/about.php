<?php
use yii\helpers\Url;
?>
<!-- Header banner -->
    <div class="container">
        <div class="jumbotron">
            <h1>Tech In Vein</h1>
            <p>Notebook for developers!</p>
            <a href="<?= Url::to('/user/login')?>" class="page-scroll btn btn-success">Start now</a>
        </div>
    </div>

<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Together we can</h2>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                </span>
                <h4>Write to learn<h4>
                <blockquote>
                    <p class="text-muted">Write your technical articles as blog, manage them all with many useful features. Need help? Ask for answers!</p>
                </blockquote>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                </span>
                <h4>Share with everyone</h4>
                <blockquote>
                    <p class="text-muted">Share your blog, help everyone to build their skills and also get some feedbacks, maybe a better solution.</p>
                </blockquote>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                </span>
                <h4>Build up community</h4>
                <blockquote>
                    <p class="text-muted">We will build up a whole community in technology which brings to you a huge resource shelter in many fields.</p>
                </blockquote>
            </div>
        </div>
    </div>
</section>
