<?php

/* @var $this yii\web\View */
$this->title = Yii::$app->name;

?>

<div class="row">
    <div class="col-md-7 col-md-offset-1">
    	<div id="main-post-list">
    		<ul class="list-unstyled">
    			<?php foreach ($posts as $post): ?>
    				<li>
    					<?= $this->render('/site/post/_item', [
    						'post' => $post
    					]) ?>
    				</li>
    			<?php endforeach; ?>
    		</ul>
    	</div>
    </div>
    <div class="col-md-4">
        <?= $this->render('_menu_right', ['latestPosts' => $latestPosts, 'topTags' => $topTags]) ?>
    </div>
</div>
