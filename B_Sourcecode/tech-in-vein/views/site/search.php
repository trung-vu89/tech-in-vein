<?php

/* @var $this yii\web\View */
$this->title = 'Search Result';
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
    	<div id="main-post-list">
    		<ul class="list-unstyled">
                <?php if (count($posts) > 0): ?>
        			<?php foreach ($posts as $post): ?>
        				<li>
        					<?= $this->render('/site/post/_item', [
        						'post' => $post
        					]) ?>
        				</li>
        			<?php endforeach; ?>
                <?php else: ?> 
                    <h4 class="well" style="width: 100%; margin: 0 auto; text-align: center;">
                        No results found.
                    </h4>
                <?php endif; ?>
    		</ul>
    	</div>
    </div>
</div>
