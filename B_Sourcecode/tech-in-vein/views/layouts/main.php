<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Tabs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="text/javascript">
        function loaded() {
            var postListWrapper = document.getElementById('main-post-list');
            var postScroll = new IScroll(postListWrapper, {
                scrollbars: true
            });
        }

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    </script>
</head>
<body onload="loaded()" style="background: #fcfcfc">

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            #--- Brand section ---#
            NavBar::begin([
                'brandLabel' => 'Tech In Vein',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            $postType = Yii::$app->request->get('type');
            echo Tabs::widget([
                'items' => [
                    ['label' => 'Articles', 'active' => (!isset($postType) || $postType == 0), 'url' => '/site/index?type=0'],
                    ['label' => 'Questions', 'active' => (isset($postType) && $postType == 1), 'url' => '/site/index?type=1'],
                ],
                'options' => ['class' => 'nav navbar-nav'],
                'navType' => 'nav-tabs-custom'
            ]);

            #--- Toolbar section ---#
            echo Nav::widget([
                'items' => [
                    '<a href="/post/create" class="btn btn-default navbar-btn btn-success">Write</a>',
                ],
                'options' => ['class' => 'navbar-nav', 'style' => 'padding-left: 10pt;']
            ]);
            echo '<form id="search-form" name="search-form" method="get" action="/site/search">';
            echo Nav::widget([
                'items' => [
                    '<input id="search-input-textbox" name="search_text" type="text" class="form-control" placeholder="Search">',
                    '<button id="search-submit-button" type="button" onclick="search(); return false;" class="btn btn-default">Search</button>',
                ],
                'options' => ['class' => 'navbar-nav navbar-form']
            ]);
            echo '</form>';

            #--- User section ---#
            $navUserItems = [];
            if (Yii::$app->user->isGuest) {
                array_push(
                    $navUserItems,
                    ['label' => 'Sign In', 'url' => '/user/login'],
                    ['label' => 'Sign Up', 'url' => '/user/register'],
                    ['label' => '', 'items' => [
                        ['label' => 'About', 'url' => '/site/about'],
                        ['label' => 'Contact', 'url' => '/site/contact']
                    ]]
                );
            } else {
                $signedInUserLabel = '<li class="dropdown-header">Signed in as %1$s</li>';
                $signedInUserLabel = sprintf($signedInUserLabel, Yii::$app->user->identity->username);
                array_push(
                    $navUserItems,
                    ['label' => Yii::$app->user->identity->username,
                     'items' => [
                            $signedInUserLabel, // DOC: https://github.com/yiisoft/yii2-bootstrap/blob/master/Nav.php
                            ['label' => 'Profile', 'url' => '/user/settings'],
                            '<li class="divider"></li>',
                            ['label' => 'Post Management', 'url' => '/post/manage'],
                            '<li class="divider"></li>',
                            ['label' => 'Sign Out', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']],
                            '<li class="divider"></li>',
                            ['label' => 'About', 'url' => '/site/about'],
                            ['label' => 'Contact', 'url' => '/site/contact'],
                        ]
                    ]
                );
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $navUserItems
            ]);

            NavBar::end();
        ?>
        <div class="container">
            <?= $content ?>
        </div>
    </div>

<footer class="footer">
  <div class="container">
      <p class="pull-left"><a href="/" style="color:black;">&copy; Tech In Vein <?= date('Y') ?></a></p>
      <ul class="pull-right list-inline">
        <li>
          <a href="<?= Url::to('/site/about')?>" style="color: black;">About</a>
        </li>
        <li>
          <a href="<?= Url::to('/site/contact')?>" style="color: black;">Contact</a>
        </li>
      </ul>
  </div>
</footer>

<!-- Modal Popup -->
<div id="modal-confirm-dialog" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="modal-title-label" class="modal-title"></h4>
            </div>
            <div id="modal-body-label" class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="modal-confirm-button" type="button" class="btn btn-primary">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
<script type="text/javascript">
    function search() {
        var searchText = document.getElementById("search-input-textbox").value;
        if (searchText.trim() != "") {
            document.getElementById("search-form").submit();
        } else {
            alert("What do you want to search?");
        }
    }
</script>
</body>
</html>
<?php $this->endPage() ?>
