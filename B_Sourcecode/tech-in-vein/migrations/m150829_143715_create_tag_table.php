<?php

use yii\db\Schema;
use app\migrations\Migration;

class m150829_143715_create_tag_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%tag}}', [
            'id'                 => Schema::TYPE_PK,
            'name'               => Schema::TYPE_STRING . '(32) NOT NULL',
            'count'              => Schema::TYPE_INTEGER . '(32)',
            'created_at'         => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'         => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $this->tableOptions);
        $this->createIndex('tag_unique_name', '{{%tag}}', 'name', true);
    }

    public function down()
    {
        echo "m150829_143715_create_tag_table should be reverted.\n";
        $this->dropTable('{{%tag}}');
    }
}
