<?php

use yii\db\Schema;
use app\migrations\Migration;

class m150729_145215_create_post_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%post}}', [
            'id'                 => Schema::TYPE_PK,
            'user_id'            => Schema::TYPE_INTEGER . ' NOT NULL',
            'title'              => Schema::TYPE_STRING . '(512) NOT NULL',
            'content'            => Schema::TYPE_TEXT . ' NOT NULL',
            'status'             => Schema::TYPE_INTEGER . '(4) NOT NULL' . ' DEFAULT \'0\'',
            'view_scope'         => Schema::TYPE_INTEGER . '(4) NOT NULL' . ' DEFAULT \'0\'',
            'type'               => Schema::TYPE_INTEGER . '(4) NOT NULL' . ' DEFAULT \'0\'',
            'comment_permission' => Schema::TYPE_INTEGER . '(4) NOT NULL' . ' DEFAULT \'0\'',
            'tags'               => Schema::TYPE_STRING . '(256)',
            'created_at'         => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'         => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $this->tableOptions);

        $this->addForeignKey('fk_user_post', '{{%post}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createIndex('post_user_id', '{{%post}}', 'id, user_id', false);
        $this->createIndex('post_title', '{{%post}}', 'title', false);
    }

    public function down()
    {
        echo "m150729_145215_create_post_table should be reverted.\n";
        $this->dropTable('{{%post}}');        
    }
}
