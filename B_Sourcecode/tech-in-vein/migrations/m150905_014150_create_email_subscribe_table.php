<?php

use yii\db\Schema;
use app\migrations\Migration;

class m150905_014150_create_email_subscribe_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%email_subscribe%}}', [
            'id'            => Schema::TYPE_PK,
            'email'         => Schema::TYPE_STRING . '(128) NOT NULL',
            'active'        => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'created_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $this->tableOptions);
        $this->createIndex('email_unique', '{{%email_subscribe%}}', 'email', true);
    }

    public function down()
    {
        echo "m150905_014150_create_email_subscribe_table should be reverted.\n";
        $this->dropTable('{{%email_subscribe%}}');
    }
}
