<?php

use yii\db\Schema;
use app\migrations\Migration;

class m150830_091512_create_comment_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%comment}}', [
            'id'                 => Schema::TYPE_PK,
            'post_id'            => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id'            => Schema::TYPE_INTEGER . ' NOT NULL',
            'content'            => Schema::TYPE_TEXT . '(1024) NOT NULL',
            'point'              => Schema::TYPE_INTEGER . '(32)',
            'created_at'         => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'         => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $this->tableOptions);

        $this->addForeignKey('fk_post_comment', '{{%comment}}', 'post_id', '{{%post}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_post_user', '{{%comment}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createIndex('comment_post_id', '{{%comment}}', 'id, post_id', false);
        $this->createIndex('comment_user_id', '{{%comment}}', 'id, user_id', false);
    }

    public function down()
    {
        echo "m150830_091512_create_comment_table should be reverted.\n";
        $this->dropTable('{{%comment}}');
    }
}
