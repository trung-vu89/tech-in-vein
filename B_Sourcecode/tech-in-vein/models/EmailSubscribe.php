<?php

namespace app\models;

use Yii;
use Yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;

class EmailSubscribe extends ActiveRecord
{
    public static function getTableName()
	{
		return '{{%email_subscribe%}}';
	}

	public function rules()
	{
		return [
            [['email', 'active'], 'required'],
			['email', 'string', 'max' => 256],
		];
	}

    public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
					ActiveRecord::EVENT_BEFORE_DELETE => 'updated_at',
				],
			],
		];
	}

    public function subscribe()
    {
        if (!$this->getIsNewRecord()) return false;
        if (!isset($this->email)) return false;
        $existSubcribe = EmailSubscribe::findOne(['email' => $this->email]);
        if (isset($existSubcribe)) return true;
        return $this->save();
    }

    public function unsubscribe($email)
    {
        if (!isset($email)) return false;
        $existSubcribe = EmailSubscribe::findOne(['email' => $email]);
        if (!isset($existSubcribe)) return false;
        $existSubcribe->active = 0;
        return $existSubcribe->update();
    }
}
