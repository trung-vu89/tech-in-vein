<?php

namespace app\models;

use Yii;
use Yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;
use dektrium\user\models\User;

class Comment extends ActiveRecord
{
    // Get table name
	public static function getTableName()
	{
		return '{{%comment}}';
	}

	public function rules()
	{
		return [
            [['post_id', 'user_id', 'content'], 'required'],
			['content', 'string', 'max' => 1024]
		];
	}

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
					ActiveRecord::EVENT_BEFORE_DELETE => 'updated_at',
				],
			],
		];
	}

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function create()
    {
        if ($this->getIsNewRecord() == false)
        {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing tag');
        }
        return $this->save();
    }
}
