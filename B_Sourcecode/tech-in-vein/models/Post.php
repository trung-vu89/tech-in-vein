<?php

namespace app\models;

use Yii;
use Yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;
use dektrium\user\models\User;

class Post extends ActiveRecord
{
	// view scope
	const SCOPE_PUBLIC  = 0;
	const SCOPE_FRIEND  = 1;
	const SCOPE_PRIVATE = 2;

	// type
	const TYPE_ARTICLE  = 0;
	const TYPE_QUESTION = 1;

	// comment permission
	const COMMENT_PUBLIC = 0;
	const COMMENT_FRIEND = 1;
	const COMMENT_LOCK   = 2;

	// post status
	const STATUS_PUBLISH = 0;
	const STATUS_DRAFT 	 = 1;
	const STATUS_DELETE  = 2;

	// Get table name
	public static function getTableName()
	{
		return '{{%post%}}';
	}

	public function rules()
	{
		return [
			['title', 'required', 'message' => 'Title is blank'],
			['title', 'string', 'max' => 512],
			['content', 'required', 'message' => 'Content is blank'],
			['tags', 'string', 'max' => 256],
			[['status', 'view_scope', 'type', 'comment_permission'], 'required'],
		];
	}

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
					ActiveRecord::EVENT_BEFORE_DELETE => 'updated_at',
				],
			],
		];
	}

	public function create()
	{
		if ($this->getIsNewRecord() == false)
		{
			throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing post');
		}
		if (empty($this->title)) { $this->title = "..."; }
		if (empty($this->content)) { $this->content = "Content is blank"; }
		if (empty($this->status) || $this->status < self::STATUS_PUBLISH || $this->status > self::STATUS_DELETE) { $this->status = self::STATUS_PUBLISH; }
		if (empty($this->view_scope) || $this->view_scope < self::SCOPE_PUBLIC || $this->view_scope > self::SCOPE_PRIVATE) { $this->view_scope = self::SCOPE_PUBLIC; }
		if (empty($this->type) || $this->type < self::TYPE_ARTICLE || $this->type > self::TYPE_QUESTION) { $this->type = self::TYPE_ARTICLE; }
		if (empty($this->comment_permission) || $this->comment_permission < self::COMMENT_PUBLIC || $this->comment_permission > self::COMMENT_LOCK) { $this->comment_permission = self::COMMENT_PUBLIC; }
		$this->user_id = Yii::$app->user->id;
		if ($this->status == self::STATUS_PUBLISH)
		{
			$this->setTags(Tag::COUNT_UP);
		}
		return $this->save();
	}

	public function edit($updatedAttributes)
	{
		if ($this->getIsNewRecord())
		{
			throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on new post');
		}
		foreach ($updatedAttributes as $name => $value) {
			$this->$name = $value;
		}
		if ($this->status == self::STATUS_PUBLISH)
		{
			$this->setTags(Tag::COUNT_UP);
		}
		else {
			$this->setTags(Tag::COUNT_DOWN);
		}
		return $this->update();
	}

	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getComments()
	{
		return $this->hasMany(Comment::className(), ['post_id' => 'id'])->orderBy('created_at DESC');
	}

	public function getTags()
	{
		if (empty($this->tags)) return array();
		return explode(',', $this->tags);
	}

	public function getAllPosts()
	{
		return Post::find()
			->with('comments')
			->where([
	            'status'     => Post::STATUS_PUBLISH,
	            'view_scope' => Post::SCOPE_PUBLIC,
	            'type'       => (isset($postType) && ($postType == Post::TYPE_ARTICLE || $postType == Post::TYPE_QUESTION)) ? (int) $postType : Post::TYPE_ARTICLE])
			->orderBy('updated_at DESC')
			->all();
	}

	public static function getAllPostsByUser($userId)
	{
		return Post::find()
			->with('comments')
			->where('user_id = :user_id', ['user_id' => (int) $userId])
			->orderBy('updated_at DESC')
			->all();
	}

	public function getRelatedPosts($targetUserId, $targetPostId, $inputType)
	{
		$allPostsByUser = Post::find()
			->with('comments')
			->where(
				'id != :id and user_id = :user_id and status = :status and view_scope = :view_scope and type = :type',
				['id' => (int) $targetPostId, 'user_id' => (int) $targetUserId, 'status'  => (int) self::STATUS_PUBLISH, 'view_scope' => (int) self::SCOPE_PUBLIC, 'type' => (int) $inputType])
			->limit(10)
			->all();
		return $allPostsByUser;
	}

	public function getLatestPost()
	{
		return Post::find()
			->with('comments')
			->where('status = :status and view_scope = :view_scope', ['status'  => (int) self::STATUS_PUBLISH, 'view_scope' => (int) self::SCOPE_PUBLIC])
			->orderBy('created_at DESC')
			->limit(10)
			->all();
	}

	public function getPostsByTag($tagName)
	{
		return Post::find()
			->with('comments')
			->where('status = :status and view_scope = :view_scope and tags like "%' . $tagName . '%"', ['status'  => (int) self::STATUS_PUBLISH, 'view_scope' => (int) self::SCOPE_PUBLIC])
			->orderBy('updated_at DESC')
			->all();
	}

	private function setTags($countingType)
	{
		$tagStrings = $this->getTags();
		if (count($tagStrings) > 0)
		{
			foreach ($tagStrings as $tagString)
			{
				$tag = Tag::findOne(['name' => Tag::makeupTag($tagString)]);
				if (isset($tag))
				{
					$tag->counting($countingType);
				}
				else
				{
					$tag = Yii::createObject(Tag::className());
					$tag->name = Tag::makeupTag($tagString);
					$tag->count = ($countingType == Tag::COUNT_UP) ? 1 : 0;
					$tag->create();
				}
			}
		}
	}
}
