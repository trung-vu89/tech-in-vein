<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Post;

class PostEditForm extends Model
{
	const SCENARIO_CREATE = 'create';
	const SCENARIO_EDIT = 'edit';

	public $id; // edit mode
	public $title;
	public $content;
	public $tags;
	public $status;
	public $view_scope;
	public $type;
	public $comment_permission;

	private $_postId;

	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_CREATE] = ['user_id', 'title', 'content', 'tags', 'status', 'view_scope', 'type', 'comment_permission'];
		$scenarios[self::SCENARIO_EDIT] = ['id', 'user_id', 'title', 'content', 'tags', 'status', 'view_scope', 'type', 'comment_permission'];
		return $scenarios;
	}

	public function rules()
	{
		return [
			[['title', 'content', 'status', 'view_scope', 'type', 'comment_permission'], 'required', 'on' => self::SCENARIO_CREATE],
			[['id', 'title', 'content', 'status', 'view_scope', 'type', 'comment_permission'], 'required', 'on' => self::SCENARIO_EDIT],
			['title', 'string', 'max' => 512],
			['tags', 'string', 'max' => 256],
		];
	}

	public function create()
	{
		if (!$this->validate())
		{
			return false;
		}
		$post = Yii::createObject(Post::className());
		$this->trimText();
		$this->loadAttributes($post);
		if (!$post->create())
		{
			Yii::$app->session->setFlash('danger', 'An error has occurred. Please try again or contact support team.');
			return false;
		}
		$this->_postId = $post->id;
		return true;
	}

	public function edit()
	{
		if (!$this->validate())
		{
			return false;
		}
		$post = Post::findOne($this->id);
		if (isset($post) &&
			$post->user_id == Yii::$app->user->id &&
			($post->status == Post::STATUS_PUBLISH || $post->status == Post::STATUS_DRAFT))
		{
			$this->trimText();
			if ($post->edit(array(
				'title' 			 => $this->title,
				'content' 			 => $this->content,
				'tags' 				 => $this->tags,
				'status'			 => $this->status,
				'view_scope'		 => $this->view_scope,
				'type'				 => $this->type,
				'comment_permission' => $this->comment_permission
				)))
			{
				$this->_postId = $post->id;
				return true;
			}
		}
		Yii::$app->session->setFlash('danger', 'An error has occurred. Please try again or contact support team.');
		return false;
	}

	public function getPostId()
	{
		return $this->_postId;
	}

	public function loadAttributesFromPost(Post $post)
	{
		$this->id = $post->id;
		$this->title = $post->title;
		$this->content = $post->content;
		$this->tags = $post->tags;
		$this->status = $post->status;
		$this->view_scope = $post->view_scope;
		$this->type = $post->type;
		$this->comment_permission = $post->comment_permission;
	}

	private function trimText()
	{
		$this->title = trim($this->title, " ");
		$this->content = trim($this->content, " ");
		$this->content = str_replace("<p><img", "<p class='text-center'><img style='max-width:100%; max-height:100%;'", $this->content);
		$this->tags = trim($this->tags, " ");
	}

	private function loadAttributes(Post $post)
	{
		$post->setAttributes($this->attributes);
	}
}
