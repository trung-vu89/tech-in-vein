<?php

namespace app\models;

use Yii;
use Yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;

class Tag extends ActiveRecord
{
    const COUNT_UP = 1;
    const COUNT_DOWN = -1;

    // Get table name
	public static function getTableName()
	{
		return '{{%tag%}}';
	}

	public function rules()
	{
		return [
            ['name', 'required', 'message' => 'Name cannot be blank.'],
			['name', 'string', 'max' => 32]
		];
	}

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
					ActiveRecord::EVENT_BEFORE_DELETE => 'updated_at',
				],
			],
		];
	}

    public function create()
    {
        if (!$this->getIsNewRecord())
        {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing tag');
        }
        if (empty($this->name)) return false;
        if ($this->count < 0)
        {
            $this->count = 0;
        }
        $this->name = $this->makeupTag($this->name);
        return $this->save();
    }

    public function counting($countingType)
    {
        if ($this->getIsNewRecord())
        {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on new tag');
        }
        if ($countingType == self::COUNT_UP)
        {
            $this->count = $this->count + 1;
        }
        elseif ($countingType == self::COUNT_DOWN)
        {
            $this->count = $this->count - 1;
        }
        if ($this->count <= 0) $this->count = 0;
        return $this->update();
    }

    public function getTopTags()
    {
        return Tag::find()
            ->where('count > :count', ['count' => 0])
			->orderBy('count DESC')
			->limit(10)
			->all();
    }

    public function makeupTag($tag)
    {
        if (!isset($tag))
        {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on invalid tag');
        }
        $editedTag = preg_replace("/[^A-Za-z0-9\#\- ]/", '', $tag);
        return strtolower($editedTag);
    }
}
