<?php

namespace app\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\AccessControl;

/**
* All actions of Image Upload & Process
*/
class ImageController extends Controller
{
    public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					['allow' => true, 'actions' => ['upload', 'delete'], 'roles' => ['@']],
				],
			],
			'verbs' => [
	            'class' => \yii\filters\VerbFilter::className(),
	            'actions' => [
	                'fbauthorize'  => ['post'],
	            ],
	        ]
		];
	}

    public function actionUpload()
    {
        if (!Yii::$app->request->isAjax || !Yii::$app->request->isPost) return false;
        if (isset($_GET['files'])) return false;
        $file = $_FILES[0];
        $fileName = $file['tmp_name'];
        $fileHandle = fopen($fileName, "r");
        $data = fread($fileHandle, filesize($fileName));
        $requestData = array('image' => base64_encode($data), 'title' => $file['name']);
        $timeout = 30;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . \Yii::$app->params['imgurClientID']));
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestData);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $out = curl_exec($curl);
        curl_close($curl);
        $pms = json_decode($out,true);
        $url = $pms['data']['link'];
        \Yii::$app->response->format = 'json';
        if($url != ""){
            return ['imageId' => $pms['data']['id'], 'imageUrl' => $url, 'deletehash' => $pms['data']['deletehash']];
        }else{
            return ['error' => $pms['data']['error']];
        }
    }

    public function actionDelete()
    {
        if (!Yii::$app->request->isAjax || !Yii::$app->request->isDelete) return false;
        $rawData = Yii::$app->request->getRawBody();
		$requestData = (Array)json_decode($rawData);
		$imageDeleteHash = $requestData['delete_hash'];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image/' . $imageDeleteHash);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . \Yii::$app->params['imgurClientID']));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $out = curl_exec($curl);
        curl_close($curl);
        $pms = json_decode($out,true);
        \Yii::$app->response->format = 'json';
        return ['success' => $pms['success'], 'status' => $pms['status']];
    }
}
