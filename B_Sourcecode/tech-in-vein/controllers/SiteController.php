<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\ContactForm;

use app\models\Post;
use app\models\Tag;
use app\models\EmailSubscribe;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        if (!Yii::$app->request->getIsGet())
        {
            return $this->render('/site/error', [
                'name' => 'Invalid request action',
                'message' => 'Your request action cannot be processed!'
            ]);
        }

        $postType = Yii::$app->request->get('type');
        $allPosts = Post::find()->with('comments')->where([
            'status'     => Post::STATUS_PUBLISH,
            'view_scope' => Post::SCOPE_PUBLIC,
            'type'       => (isset($postType) && ($postType == Post::TYPE_ARTICLE || $postType == Post::TYPE_QUESTION)) ? (int) $postType : Post::TYPE_ARTICLE
        ])->orderBy('updated_at DESC')->all();

        return $this->render('index', [
            'posts' => $allPosts,
            'latestPosts' => Post::getLatestPost(),
            'topTags' => Tag::getTopTags()
        ]);
    }

    public function actionSearch()
    {
        if (!Yii::$app->request->isGet) return false;
        $searchText = Yii::$app->request->get('search_text');
        if (isset($searchText)) {
            return $this->render('/site/search', [
                'posts' => [],
            ]);
        }
        $searchPosts = Post::find()->with('comments')->where(
            'status = ' . Post::STATUS_PUBLISH . ' and view_scope = ' . Post::SCOPE_PUBLIC . ' and (title like "%' . $searchText . '%" or content like "%' . $searchText . '%" or tags like "%' . $searchText . '%")'
        )->orderBy('updated_at DESC')->all();

        return $this->render('/site/search', [
            'posts' => $searchPosts,
        ]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionSubscribe()
    {
        if (!Yii::$app->request->isAjax || !Yii::$app->request->isPost) return false;
        $rawData = Yii::$app->request->getRawBody();
		$requestData = (Array)json_decode($rawData);
		$emailAddress = $requestData['email'];
        if (!isset($emailAddress)) return false;
        if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) return false;
        $subscribe = Yii::createObject(EmailSubscribe::className());
        $subscribe->email = $emailAddress;
        $subscribe->active = 1;
        return $subscribe->subscribe();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
