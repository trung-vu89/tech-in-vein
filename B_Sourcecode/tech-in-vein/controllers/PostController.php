<?php

namespace app\controllers;

use Yii;
use app\models\Post;
use app\models\Comment;
use app\models\PostEditForm;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
* All actions of Post
*/
class PostController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					['allow' => true, 'actions' => ['create', 'manage', 'publish', 'delete', 'edit', 'comment'], 'roles' => ['@']],
					['allow' => true, 'actions' => ['view', 'tag'], 'roles' => ['?', '@']],
				],
			],
			'verbs' => [
	            'class' => \yii\filters\VerbFilter::className(),
	            'actions' => [
	                'fbauthorize'  => ['post'],
	            ],
	        ]
		];
	}

	public function actionView()
	{
		if (!Yii::$app->request->getIsGet())
		{
			return $this->render('/site/error', [
				'name' => 'Invalid request action',
				'message' => 'Your request action cannot be processed!'
			]);
		}
		$postId = Yii::$app->request->getQueryParam('id');
		if (empty($postId))
		{
			return $this->render('/site/error', [
				'name' => 'Invalid request action',
				'message' => 'Your request action cannot be processed!'
			]);
		}
		$model = Post::findOne($postId);
		if (empty($model) || ($model->status != Post::STATUS_PUBLISH && $model->user->id != Yii::$app->user->id))
		{
			return $this->render('/site/error', [
				'name' => 'Information not found',
				'message' => 'Post information cannot found!',
			]);
		}
		$model->content = $this->setCodeTag($model);
		return $this->render('/site/post/view', [
			'model' => $model,
			'profile' => $model->user->profile,
			'relatedPosts' => $model->getRelatedPosts($model->user->id, $postId, $model->type),
		]);
	}

	public function actionCreate()
	{
		// create new post form
		$model = new PostEditForm(['scenario' => PostEditForm::SCENARIO_CREATE]);

		if (Yii::$app->request->isPost)
		{
			$model->attributes = Yii::$app->request->post('PostEditForm');
			if ($model->create())
			{
				$viewUrl = Url::to(['/post/view', 'id' => $model->getPostId()]);
				return $this->redirect($viewUrl);
			}
		}

		$model->type = Post::TYPE_ARTICLE; // article
		$model->status = Post::STATUS_PUBLISH; // publish post
		return $this->render('/site/post/edit', [
			'model' => $model
		]);
	}

	public function actionEdit()
	{
		$model = new PostEditForm(['scenario' => PostEditForm::SCENARIO_EDIT]);

		if (Yii::$app->request->isGet)
		{
			$postId = Yii::$app->request->getQueryParam('id');
			$post = Post::findOne($postId);
			if (isset($post) &&
				$post->user_id == Yii::$app->user->id &&
				($post->status == Post::STATUS_PUBLISH || $post->status == Post::STATUS_DRAFT))
			{
				$model->loadAttributesFromPost($post);
				return $this->render('/site/post/edit', [
					'model' => $model
				]);
			}
		}

		if (Yii::$app->request->isPut)
		{
			$model->attributes = Yii::$app->request->post('PostEditForm');
			if ($model->edit())
			{
				$viewUrl = Url::to(['/post/view', 'id' => $model->getPostId()]);
				return $this->redirect($viewUrl);
			}
			return $this->render('/site/post/edit', [
				'model' => $model
			]);
		}

		return $this->render('/site/error', [
			'name' => 'Information not found',
			'message' => 'Post information cannot found!',
		]);
	}

	public function actionManage()
	{
		$allPostsByUser = Post::getAllPostsByUser(Yii::$app->user->id);
		return $this->render('/site/post/manage', [
			'posts' => $allPostsByUser
		]);
	}

	public function actionPublish()
	{
		if (!Yii::$app->request->isAjax || !Yii::$app->request->isPut) return false;
		// use getRawBody() or file_get_contents("php://input")
		$rawData = Yii::$app->request->getRawBody();
		$requestData = (Array)json_decode($rawData);
		$postId = $requestData['post_id'];
		if (!isset($postId)) return false;
		$post = Post::findOne($postId);
		if (!isset($post) || $post->status == Post::STATUS_PUBLISH) return false;
		return $post->edit(array('status' => Post::STATUS_PUBLISH));
	}

	public function actionDelete()
	{
		if (!Yii::$app->request->isAjax || !Yii::$app->request->isDelete) return false;
		$rawData = Yii::$app->request->getRawBody();
		$requestData = (Array)json_decode($rawData);
		$postId = $requestData['post_id'];
		if (!isset($postId)) return false;
		$post = Post::findOne($postId);
		if (!isset($post) || $post->status == Post::STATUS_DELETE) return false;
		return $post->edit(array('status' => Post::STATUS_DELETE));
	}

	public function actionTag()
	{
		if (!Yii::$app->request->isGet)
		{
			return $this->render('/site/error', [
				'name' => 'Invalid request action',
				'message' => 'Your request action cannot be processed!'
			]);
		}
		$tagName = Yii::$app->request->getQueryParam('name');
		$postsByTag = Post::getPostsByTag($tagName);
		return $this->render('/site/post/tag', [
			'postsByTag' => $postsByTag
		]);
	}

	public function actionComment()
	{
		if (!Yii::$app->request->isAjax || !Yii::$app->request->isPost) return false;
		$rawData = Yii::$app->request->getRawBody();
		$requestData = (Array)json_decode($rawData);
		$postId = $requestData['post_id'];
		$comment = $requestData['comment'];
		if (!isset($postId) || !isset($comment)) return false;
		$commentData = Yii::createObject(Comment::className());
		$commentData->post_id = $postId;
		$commentData->user_id = Yii::$app->user->id;
		$commentData->content = $comment;
		if ($commentData->create())
		{
			\Yii::$app->response->format = 'json';
			return $this->commentAttributes($commentData);
		}
		return false;
	}

	// Replace origin <pre> tag for custom Prism code tag
	private function setCodeTag(Post $post)
	{
		$postContent = $post->content;
		if (!empty($postContent))
		{
			if (strpos($postContent,'<pre>') && strpos($postContent,'</pre>'))
			{
				$postContent = str_ireplace('<pre>', '<pre><code class="language-c">', $postContent);
				$postContent = str_ireplace('</pre>', '</code></pre>', $postContent);
			}
		}
		return $postContent;
	}

	private function commentAttributes(Comment $comment)
	{
		return [
			'id'  		 => $comment->id,
			'post_id'    => $comment->post_id,
			'user_id'    => $comment->user_id,
			'comment'    => $comment->content,
			'point'      => $comment->point,
			'created_at' => $comment->created_at,
			'updated_at' => $comment->updated_at
		];
	}
}
